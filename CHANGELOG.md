# CHANGELOG

## 0.1.6 (2024-12-19)

### New

*   Added minimum window size option to windows function.

## 0.1.5 (2024-12-13)

### New

*   Change folder structure.
*   Added type hinting.

## 0.1.4 (2024-11-13)

### Fixed

*   Added distribution statement.

## 0.1.3 (2024-11-11)

### New

*   File-level doc.

## 0.1.2 (2024-08-19)

### Fixed

*   Removed metadata from `avar.py` in favor of `pyproject.toml` metadata.
